﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PlayerInput : MonoBehaviour
{
    private Vector3 from, to, size;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            from = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {
            to = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            DemoController.Instance().SetSplitObject(from, to);
        }

        if(Input.GetKeyDown(KeyCode.C))
        {
            DemoController.Instance().DestroySpawnedMesh();
        }

        if(Input.GetKeyDown(KeyCode.R))
        {
            DemoController.Instance().CreateRandom3DMesh();
        }else if(Input.GetKeyDown(KeyCode.T))
        {
            DemoController.Instance().CreateRandomRoughPlane();
        }

        float val = -Input.GetAxis("Horizontal");
        DemoController.Instance().OnRotateAround(val);
    }

    void OnPostRender()
    {
        GLLine(from, to);
    }
    void GLLine(Vector2 from, Vector2 to)
    {
        size = new Vector2(Screen.width, Screen.height);
        GL.PushMatrix();
        GL.LoadOrtho();
        GL.Begin(GL.LINES);
        GL.Color(Color.red);
        GL.Vertex3(from.x / size.x, from.y / size.y, DemoController.Instance().mainCamera.nearClipPlane);
        GL.Vertex3(to.x / size.x, to.y / size.y, DemoController.Instance().mainCamera.nearClipPlane);
        GL.End();
        GL.PopMatrix();
    }
}