﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EzySlice;
using Dynagon;
using System.Linq;

[RequireComponent(typeof(Camera))]
public class DemoController : MonoBehaviour
{
    public Camera mainCamera = null;
    public Material demoMat = null;

    private Vector3[] default_vertices = new Vector3[20];   

    private static DemoController instance = null;

    public static DemoController Instance()
    {
        return instance;
    }

    void Start ()
    {
        instance = this;
        this.initialize();
	}

    private void Update()
    {

    }

    public void DestroySpawnedMesh()
    {
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("spawn"))
        {
            Destroy(obj);
        }
    }

    public void CreateRandom3DMesh()
    {
        int length = this.default_vertices.Length;
        
        for(int index=0;index<length;++index)
        {
            this.default_vertices[index] = Random.insideUnitSphere * 5;
        }
        this.DestroySpawnedMesh();
        this.create3DMesh();
    }

    public void CreateRandomRoughPlane()
    {
        this.DestroySpawnedMesh();
        this.CreateRoughPlane();
    }

    public void OnRotateAround(float delta)
    {
        this.transform.RotateAround(Vector3.zero, Vector3.up, delta * 1.5f);
    }

    private void initialize()
    {
        if(!mainCamera)
        {
            mainCamera = this.GetComponent<Camera>();
            if (!mainCamera) { Debug.LogError("No cameras was found."); return; }
        }
        this.CreateRandom3DMesh();
    }

    private void CreateRoughPlane(float tileSize = 3f, int numTiles = 5)
    {
        var half = (tileSize * numTiles) / 2;
        var offset = new Vector3(-half, -half, 0f);
        var vertices = new List<Vector3>();
        foreach (var i in Enumerable.Range(0, numTiles))
        {
            foreach (var j in Enumerable.Range(0, numTiles))
            {
                var noise = Random.insideUnitSphere * tileSize / 2;
                vertices.Add(offset + new Vector3(tileSize * i + noise.x, tileSize * j + noise.y,Random.Range(0,3)));
            }
        }
        var polygon = new Polygon2D(
            new GameObject("2d_plane_demo"),
            Triangulator2D.Triangulate(vertices)
            ).Build();
        polygon.gameObject.transform.Rotate(new Vector3(-90, 0, 0));
        polygon.gameObject.GetComponent<MeshRenderer>().material = demoMat;
        polygon.gameObject.tag = "spawn";
    }

    private void create3DMesh()
    {
        List<Vector3> originVertices = new List<Vector3>();
        foreach(Vector3 vertex in default_vertices)
        {
            originVertices.Add(vertex);
        }
        GameObject meshObj = Factory.Create("airspace_demo_mesh", originVertices).gameObject;
        meshObj.GetComponent<MeshRenderer>().material = new Material(demoMat);
        meshObj.tag = "spawn";
    }

    public void SetSplitObject(Vector3 from, Vector3 to)
    {
        float near = mainCamera.nearClipPlane;
        Vector3 line = from - to;
        if (line.magnitude < 0.1f) { return; }
        float rayLength = new Vector2(line.x / Screen.width, line.y / Screen.height).magnitude;

        line = mainCamera.ScreenToWorldPoint(new Vector3(to.x, to.y, mainCamera.nearClipPlane)) - mainCamera.ScreenToWorldPoint(new Vector3(from.x, from.y, mainCamera.nearClipPlane));
        for (float length = 0; length <= rayLength; length += 0.04f)
        {
            Ray ray = mainCamera.ScreenPointToRay(Vector3.Lerp(from, to, length / rayLength));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                GameObject objectToSlice = hit.transform.gameObject;
                Vector3 planeNormal = Vector3.Cross(line.normalized, ray.direction).normalized;
                GameObject[] splits = objectToSlice.SliceInstantiate(hit.point, planeNormal,demoMat);
                if (splits ==null || splits.Length < 2) { return; }
                this.Animate(splits[0],planeNormal);
                this.Animate(splits[1], -planeNormal);
                foreach (GameObject obj in splits)
                {
                    obj.AddComponent<MeshCollider>().convex = true;
                    obj.GetComponent<MeshRenderer>().material = new Material(demoMat);
                    obj.tag = "spawn";
                }
                Destroy(objectToSlice);
                objectToSlice = null;
                break;
            }
        }
    }

    private void Animate(GameObject target,Vector3 dir)
    {
        Vector3 destination = target.transform.position + dir * 0.5f;
        iTween.MoveTo(target, iTween.Hash("position",destination , "easeType", "easeInOutExpo", "loopType", "none", "delay", .1));
    }
}
